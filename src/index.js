const request = require('request-promise-native')

class SocialminerClient {
  constructor ({host, username, password}) {
    this.host = host
    this.username = username
    this.password = password
  }

  async agentRequest ({feedId, contact}) {
    // validate input
    if (!feedId || !contact) {
      throw new Error('feedId and contact are required properties')
    }
    const qs = {
      name: contact.name,
      title: contact.title,
      description: contact.description,
      mediaAddress: contact.mediaAddress,
      tags: contact.tags
    }
    contact.variables.forEach(v => {
      qs['variable_' + v.name] = v.value
    })
    const options = {
      qs,
      resolveWithFullResponse: true
    }
    try {
      const response = await request(`https://${this.host}/ccp/callback/feed/${feedId}`, options)
      return response.headers.location
    } catch (e) {
      throw e
    }
  }

  async taskRequest ({feedId, contact}) {
    const qs = {
      name: contact.name,
      title: contact.title,
      description: contact.description,
      scriptSelector: contact.scriptSelector,
      tags: contact.tags
    }
    contact.variables.forEach(v => {
      qs['variable_' + v.name] = v.value
    })
    // set mediaType variable
    // qs['variable_mediaType'] = contact.mediaType
    // set podRefURL if exists
    // if (contact.podRefUrl) {
    //   qs['variable_podRefURL'] = contact.podRefUrl
    // }
    const options = {
      qs,
      resolveWithFullResponse: true
    }
    try {
      const response = await request(`https://${this.host}/ccp/task/feed/${feedId}`, options)
      return response.headers.location
    } catch (e) {
      throw e
    }
  }
}

module.exports = SocialminerClient
