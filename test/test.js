const smClient = require('../src/index')

const sm = new smClient({
  host: 'sm1.dcloud.cisco.com'
})

const body = {
  "feedId": "100020",
  "contact": {
    "name": "Jane Smith",
    "title": "My New Callback",
    "description": "This is an example callback request",
    "mediaAddress": "2145551123",
    "tags": "",
    "variables": [
    //   {
    //   "name": "user_user.PQID",
    //   "value": "5024"
    // }, {
    //   "name": "user_user.CTID",
    //   "value": "5064"
    // }
  ]
  }
}

async function go() {
  const results = await sm.agentRequest(body)
  // console.log(results)
  return results
}

go()
.then(r => console.log('success', r))
.catch(e => console.log('failed', e))
